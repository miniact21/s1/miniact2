let total;
let ans = document.querySelector(".result");

/*Operators*/
let addition = document.getElementById("add").addEventListener("click",add);
let subtraction = document.getElementById("sub").addEventListener("click",sub);
let multiply = document.getElementById("multip").addEventListener("click",multi);
let division = document.getElementById("div").addEventListener("click",div);
let modulo = document.getElementById("mod").addEventListener("click",mod);


let clear = document.getElementById("clear").addEventListener("click", clr);


function clr(){
    let num1 = document.getElementById("txt1").value = "";
    let num2 = document.getElementById("txt2").value = "";
}
function add(){
    let num1 = document.getElementById("txt1").value;
    let num2 = document.getElementById("txt2").value;
     total = parseFloat(num1) + parseFloat(num2);
     ans.innerHTML = total;
     clr();
}

function sub(){
    let num1 = document.getElementById("txt1").value;
    let num2 = document.getElementById("txt2").value;
     total = parseFloat(num1) - parseFloat(num2);
     ans.innerHTML = total;
     clr();
}

function multi(){
    let num1 = document.getElementById("txt1").value;
    let num2 = document.getElementById("txt2").value;
    total = parseFloat(num1) * parseFloat(num2);
    ans.innerHTML = total;
    clr();
}

function div(){
    let num1 = document.getElementById("txt1").value;
    let num2 = document.getElementById("txt2").value;
     total = parseFloat(num1) / parseFloat(num2);
     ans.innerHTML = total;
     clr();
}

function mod(){
    let num1 = document.getElementById("txt1").value;
    let num2 = document.getElementById("txt2").value;
     total = parseFloat(num1) %  parseFloat(num2);
     ans.innerHTML = total;
     clr();
}
